from bs4 import BeautifulSoup
import urllib
import re


#set up URLS
rootUrl = 'http://www.brandeis.edu/facultyguide/'


#set up file to write to
file = open('brandeis.csv', 'w')
log = open('log.txt', 'w')
file.write('First Name,Last Name,School of PhD,Department of PhD,Year of PhD,School,Department,Year Started\n')


#create root page connections
rootPage = urllib.request.urlopen(rootUrl + 'index.html')
soup = BeautifulSoup(rootPage.read())

allSchools = soup.find('div', {'id':'content'}).find('ul').findAll('a')


#function to process faculty and grab phd information
def processFaculty(allFaculty, dept):
    facultyString = '' #empty string, will be returned
    
    #loop over all faculty found
    for faculty in allFaculty:
        #split out the name
        name = faculty.string.split(',')
        fName = name[1].strip()
        lName = name[0].strip()
        
        #open the faculty member's bio page
        facultyPage = urllib.request.urlopen(rootUrl + faculty['href'])
        facultySoup = BeautifulSoup(facultyPage.read())
        
        #find the section with degree information
        educationSoup = facultySoup.find('div', {'id':'degrees'})
        
        #regex to get phd information
        phdRegex = re.search('[a-zA-Z, ]*([Pp][Hh][.][dD][.])[a-zA-Z, ]*', educationSoup.prettify())
        if phdRegex:
            #if the faculty has a phd get the location and generate the csv string
            phd = phdRegex.group(0).split(',')[0].strip()
            facultyCsv = ('"' + fName + '","' + lName + '","' + phd + '",,,' + '"Brandeis University","' + dept + '",\n')
            
            #add the csv string to the final string
            facultyString += facultyCsv
            
            #print acceptance, for debugging
            print(fName + ' ' + lName + ' accepted, phd from ' + phd)
            log.write(fName + ' ' + lName + ' accepted, phd from ' + phd + '\n')
        else:
            #print rejection in the console, not saved in the csv
            print(fName + ' ' + lName + ' rejected, no phd')
            log.write(fName + ' ' + lName + ' rejected, no phd\n')
    #return the final string
    return facultyString

#go through each school
for school in allSchools:
    print('\n==========\nstarting ' + school.string + '\n==========\n')
    log.write('\n==========\nstarting ' + school.string + '\n==========\n')
    
    #open school page
    schoolPage = urllib.request.urlopen(rootUrl + school['href'])
    schoolSoup = BeautifulSoup(schoolPage.read())
    
    #SPECIAL CASE FOR Arts and Sciences school, the next page has departments not people
    if school.string == 'Arts and Sciences':
        #find all departments
        allDepartments = schoolSoup.find('div', {'id':'content'}).findAll('a')
        
        #go through all the departments
        for department in allDepartments:
            print('\n==========\nstarting ' + department.string + '\n==========\n')
            log.write('\n==========\nstarting ' + department.string + '\n==========\n')
            
            #go to the department page
            departmentPage = urllib.request.urlopen(rootUrl + department['href'])
            departmentSoup = BeautifulSoup(departmentPage.read())
            
            #find and write the results of processing all fadulty in this department
            file.write(processFaculty(departmentSoup.find('div', {'id':'content'}).findAll('a'), department.string))
    else:
        #find and write the results of processing all fadulty in this school
        file.write(processFaculty(schoolSoup.find('div', {'id':'content'}).findAll('a'), school.string))
        

print('Done!')
log.write('Done!')